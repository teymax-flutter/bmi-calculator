import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const kBottomContainerHeight = 80.0;
const kActiveCardColour = Color(0xFF1D1E33);
const kInactiveCardColour = Color(0xFF111328);
const kThemePinkColour = Color(0xFFEB1555);
const kTransparentPinkColour = Color(0x1FEB1555);
const kThemeGreyColour = Color(0xFF8D8E98);

const kMinHeight = 120.0;
const kMaxHeight = 220.0;

const kLabelTextStyle = TextStyle(
  fontSize: 18,
  color: kThemeGreyColour,
);

const kNumberTextStyle = TextStyle(
  fontSize: 50,
  fontWeight: FontWeight.w900,
);

const kLargeButtonText = TextStyle(
  fontSize: 25,
  fontWeight: FontWeight.bold,
);

const kTitleTextStyle = TextStyle(
  fontSize: 50,
  fontWeight: FontWeight.bold,
);

const kResultTextStyle = TextStyle(
  color: Color(0xFF24D876),
  fontSize: 22,
  fontWeight: FontWeight.bold,
);

const kBMITextStyle = TextStyle(
  fontSize: 100,
  fontWeight: FontWeight.bold,
);
const kBodyTextStyle = TextStyle(fontSize: 22);
